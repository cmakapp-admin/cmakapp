/* Amplify Params - DO NOT EDIT
  AUTH_CMAKAPPF72350E2_USERPOOLID
  ENV
  REGION
  STORAGE_CMAKDYNAMO_ARN
  STORAGE_CMAKDYNAMO_NAME
Amplify Params - DO NOT EDIT */
import {
  APIGatewayProxyWithCognitoAuthorizerHandler,
  APIGatewayProxyWithCognitoAuthorizerEvent,
  APIGatewayProxyResult,
} from "aws-lambda";
import { CognitoIdentityServiceProvider, DynamoDB } from 'aws-sdk';
import { getCurrentUser, getCurrentAttendee, getAttendeeById } from "/opt/server-auth";
import { createCmakHandler, CmakAuthorizationError, Cached } from "/opt/server-tools";
import { Attendee, LetterEvaluation, Authorization, demandEstimate } from "/opt/cmak-types";
import { QuerrierIns } from "/opt/querrier"

const docClient = new DynamoDB.DocumentClient();

const uuidToId = new Cached(()=>QuerrierIns.queryAttendees()
.then(attendees=>attendees.reduce((map,a)=>{map.set(a.uuid,a.id);return map},new Map<string,string>())
));

const demand = new Cached(()=>QuerrierIns.queryAttendees()
.then((attendees=>demandEstimate(attendees))),
600);

/**
 * //   //GET ?email getEvalsbyAuthor
//   // authorization: admin || currentUser==email

//   //POST ?email getNewEvaluation for author=email

//   //PUT submitEvals 

//   //DELETE ?email&uuid email ~author uuid~Attendee.uuid
//   // authorization : admin || currentUser==email

 */
export const handler = createCmakHandler({
  GET: (event, currentUser) => branchGets(event, currentUser),
  POST: (event, currentUser) => getNewEvaluation(event.queryStringParameters.email, currentUser),
  PATCH: (event, currentUser) => submitEvaluations(currentUser, event)
})

function branchGets(event:APIGatewayProxyWithCognitoAuthorizerEvent, currentUser:Attendee){
  switch(event.queryStringParameters.mode){
    case "author":
      return getEvaluationsForAuthor(event.queryStringParameters.email, currentUser);
    case "demand":
      return getDemand();
    case "letter":
      return getMotivationLetterByUuid(event.queryStringParameters.uuid, currentUser);
  }
}

async function getDemand():Promise<Array<[string , number]>> {
  return demand.get()
}

async function submitEvaluations(currentUser: Attendee, event: APIGatewayProxyWithCognitoAuthorizerEvent) {
  const evaluator = currentUser

  if (evaluator.authorizations?.includes(Authorization.EVALUATOR)) {
    // const attendees = await QuerrierIns.queryAttendees()
    let evaluations: { [key: string]: LetterEvaluation } = JSON.parse(
      event.body ?? "{}"
    );
    let promises: Promise<any>[] = [];

    Object.entries(evaluations).forEach(([uuid, evaluation]) => {
      promises.push(
        uuidToId
          .get()
          .then((map) => getAttendeeById(map.get(uuid)))
          .then(theAttendee => {
            if (theAttendee) {
              const i = theAttendee.motivationLetterRatings.findIndex(
                (le) => le.author.email === evaluation.author!.email
              );
              theAttendee.motivationLetterRatings.splice(i, 1, evaluation);
              //push to dynamo
              let params = {
                TableName: process.env.STORAGE_CMAKDYNAMO_NAME!,
                Key: {
                  id: theAttendee.id,
                },
                UpdateExpression:
                  "SET #motivationLetterRatings = :motivationLetterRatings",
                  ExpressionAttributeNames: {"#motivationLetterRatings":"motivationLetterRatings"},
                ExpressionAttributeValues: {
                  ":motivationLetterRatings":
                    theAttendee.motivationLetterRatings,
                },
                ReturnValues: "NONE",
              };
              console.log(JSON.stringify(params));
              return docClient.update(params).promise();
            }
          })
      );
    });
    return Promise.all(promises);
  } else {
    throw new CmakAuthorizationError("Musíš být Hodnotitel");
  }
}

async function getEvaluationsForAuthor(email: string | undefined, currentUser: Attendee): Promise<{ [key: string]: LetterEvaluation }> {
  const evaluator = currentUser

  if (evaluator.authorizations?.includes(Authorization.EVALUATOR)) {
    const attendees = await QuerrierIns.queryAttendees()

    return findEvaluations(evaluator, attendees)
  } else {
    throw new CmakAuthorizationError("Musíš být Hodnotitel");
  }
}

async function getMotivationLetterByUuid(uuid: string, currentUser: Attendee) {
  const caller = currentUser

  if (caller.authorizations.includes(Authorization.EVALUATOR || Authorization.ADMIN)) {
    let theAttendee = uuidToId.get().then((map) => getAttendeeById(map.get(uuid)))
    return (await theAttendee).registrationInfo.motivationalLetter
  } else {
    throw new CmakAuthorizationError("Nemáš dostatečné oprávnění");
  }
}

async function getNewEvaluation(email: string | undefined, currentUser: Attendee): Promise<[string, LetterEvaluation] | undefined> {
  const evaluator = currentUser

  if (evaluator.authorizations?.includes(Authorization.EVALUATOR)) {
    const attendeeToRate = findWithoutRatings(findWithLetters(await QuerrierIns.queryAttendees()), evaluator);
    const newEval: LetterEvaluation = { grammar: 0, creativity: 0, motivation: 0, karasIndex: 0, author: evaluator }
    let params: DynamoDB.DocumentClient.UpdateItemInput
    if (!attendeeToRate) {
      return undefined
    }
    if (attendeeToRate.motivationLetterRatings) {
      params = {
        TableName: process.env.STORAGE_CMAKDYNAMO_NAME!,
        Key: {
          id: attendeeToRate.id
        },
        UpdateExpression: 'SET #motivationLetterRatings = list_append(#motivationLetterRatings, :motivationLetterRatings)',
    ExpressionAttributeNames: {"#motivationLetterRatings":"motivationLetterRatings"},
        ExpressionAttributeValues: {
          ':motivationLetterRatings': [newEval]
        },
        ReturnValues: 'ALL_NEW'
      }

    } else {

      params = {
        TableName: process.env.STORAGE_CMAKDYNAMO_NAME!,
        Key: {
          id: attendeeToRate.id
        },
        UpdateExpression: 'SET #mlr = :motivationLetterRatings',
        ExpressionAttributeNames: {
          "#mlr": "motivationLetterRatings"
        },
        ExpressionAttributeValues: {
          ':motivationLetterRatings': [newEval]
        },
        ReturnValues: 'ALL_NEW'
      }
    }

    console.log(JSON.stringify(params));
    return await docClient.update(params)
    .promise()
    .then((r) => {
      return [attendeeToRate.uuid, newEval];
    })
    //return [attendeeToRate.uuid, newEval]
  } else {
    throw new Error("Access forbidden")
  }
}

function findWithLetters(atts: Attendee[]): Attendee[] {

  return atts.filter(a => a.registrationInfo?.motivationalLetter)
}

function findWithoutRatings(atts: Attendee[], user: Attendee): Attendee | undefined {
  return atts.find(a => {
    return !a.motivationLetterRatings || (a.motivationLetterRatings.length < 3) && !(a.motivationLetterRatings.find(r => r.author.email === user.email))
  })
}

function isAuthor(author: Attendee, target: Attendee): boolean {
  target.motivationLetterRatings.forEach(rating => {
    if (rating.author.email == author.email) {
      return true
    }

  })
  return false
}

function findEvaluations(author: Attendee, attendees: Attendee[]): { [key: string]: LetterEvaluation } {
  return attendees.map(a => {
    return {
      uuid: a.uuid,
      evaluation: a.motivationLetterRatings?.find(r => (r.author?.email === author.email))
    }
  })
    .filter(d => d.evaluation)
    .reduce((result, d) => {
      result[d.uuid] = d.evaluation;
      return result
    }, {} as { [key: string]: LetterEvaluation })
}
