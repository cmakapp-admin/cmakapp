export interface CmakConfig {
  /**
   * adding new user ercords upon google login
   */
  signUpAllowed: boolean;

  /**
   * changing motivation letter / registration
   */
  registrationsAllowed: boolean;

  /**
   * keep assigned roles hidden from users
   */
  hideAssignedRoles: boolean;

  //Allows sending out of emails
  emailsAllowed: boolean;

  apply(update: Partial<CmakConfig>);
}

export class CmakConfigImpl implements CmakConfig {
  signUpAllowed=true;
  registrationsAllowed=true;
  hideAssignedRoles=true;
  emailsAllowed=true;


  apply(patch: Partial<CmakConfig>) {
    Object.entries(patch).forEach(([key, value]) => {
      this[key] = value;
    });
  }
  
  constructor(patch?: Partial<CmakConfig>) {
    if (patch) {
      this.apply(patch);
    }
  }
}

export const config: CmakConfig = new CmakConfigImpl();
