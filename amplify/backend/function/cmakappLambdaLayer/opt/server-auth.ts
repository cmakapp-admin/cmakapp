import { User, Attendee, Authorization } from "./cmak-types";
import { APIGatewayProxyWithCognitoAuthorizerEvent } from "aws-lambda";
import { DynamoDB, CognitoIdentityServiceProvider } from "aws-sdk";
import { createCmakHandler, CmakAuthorizationError, CmakNotFoundError, serverConfigProvider } from "./server-tools";
import { v4 as uuid, v4 } from "uuid";

export function getUserId(
  event: APIGatewayProxyWithCognitoAuthorizerEvent
): string {
  const IDP_REGEX = /.*\/.*,(.*)\/(.*):CognitoSignIn:(.*)/;
  const authProvider = event.requestContext.identity
    .cognitoAuthenticationProvider as string;
  const userId = authProvider.match(IDP_REGEX)![3];
  return userId;
}

export function getAttendeeById(id: string): Promise<Attendee | undefined> {
  const docClient = new DynamoDB.DocumentClient();
  const params = {
    TableName: process.env.STORAGE_CMAKDYNAMO_NAME!,
    Key: {
      id: id,
    },
  };

  return docClient
    .get(params)
    .promise()
    .then((r) => {
      if (r.Item) {
        return r.Item as Attendee;
      }
    });
}

export function getCurrentAttendee(
  event: APIGatewayProxyWithCognitoAuthorizerEvent
): Promise<Attendee | undefined> {
  const docClient = new DynamoDB.DocumentClient();
  return getAttendeeById(getUserId(event)).then((attendee) => {
    if (!attendee) {
      //user is not there, let's create him
      return serverConfigProvider.get().then(config=>{
        if (!config.signUpAllowed)
        throw new CmakAuthorizationError("Nové registrace nejsou povoleny")
      }).then(()=>getCurrentUser(event).then((currentUser) => {
        const newAttendee: Attendee = {
          id: getUserId(event),
          uuid: uuid(),
          ...currentUser,
        };

        const put: DynamoDB.PutItemInput = {
          TableName: process.env.STORAGE_CMAKDYNAMO_NAME!,
          Item: (newAttendee as unknown) as DynamoDB.PutItemInputAttributeMap,
        };
        return docClient
          .put(put)
          .promise()
          .then(() => newAttendee);
      }));
    } else {
      return serverConfigProvider.get().then(config=>{
        attendee = filterAtts(attendee!,attendee!)
        if (config.hideAssignedRoles){
          attendee.position = undefined
        }
        return attendee
      });
    }
  });;
}

export function getCurrentUser(
  event: APIGatewayProxyWithCognitoAuthorizerEvent
): Promise<User> {
  const userId = getUserId(event);
  if (!process.env.AUTH_CMAKAPPF72350E2_USERPOOLID) {
    throw "userpool id not provided in env";
  }
  const cognito = new CognitoIdentityServiceProvider();
  return cognito
    .listUsers({
      UserPoolId: process.env.AUTH_CMAKAPPF72350E2_USERPOOLID,
      Filter: `sub = "${userId}"`,
    })
    .promise()
    .then((result) => {
      let user = result.Users![0];
      if (user) {
        let attribs = new Map(
          user.Attributes!.map((attr) => [attr.Name, attr.Value ?? ""])
        );
        return {
          email: attribs.get("email") ?? "",
          name: attribs.get("name") ?? "",
          pic: attribs.get("picture") ?? "",
          id: userId,
        };
      } else {
        throw `User not found for ${userId}`;
      }
    });
}

export function filterAtts(thisAtt:Attendee, requester:Attendee):Attendee 
{
  
    if(requester.authorizations?.includes(Authorization.ADMIN)){
      return thisAtt
    }
    else {      
      thisAtt.motivationLetterRatings = undefined;
      return thisAtt
   }        
  }
