import { Attendee, Authorization, FinanceInfo, feeGenerator, RatingResponsibilities, Position, PositionFilter, 
    ApplicableFilter, Rating, GameRating, DailyRating, isWithinFilters, User, Person } from "./cmak-types";

export function getValidatedGameRating(ratedAttendee:Attendee):GameRating{
  if (!ratedAttendee.gameRating) {
    ratedAttendee.gameRating = { dailyRatings: new Array<DailyRating[]>([], [], []) }
  }
  return ratedAttendee.gameRating
}

export function getValidatedDailyRatingPatch(ratedAttendeeId:string, gameRatingToUpdate:GameRating, author:Person, day:number, newRating:Rating ):Partial<Attendee>{
    let patch: Partial<Attendee> = { id: ratedAttendeeId }
          patch.gameRating = gameRatingToUpdate
          //Condition below is used to change the rating, should it already exist, instead of making a new one
          if (gameRatingToUpdate.dailyRatings[day].find(rating => rating.judge = author)) {
            const index = gameRatingToUpdate.dailyRatings[day].indexOf(gameRatingToUpdate.dailyRatings[day].find(rating => rating.judge = author)!)
            patch.gameRating.dailyRatings[day][index]={
              judge:author,
              rating:newRating
            }
          } else {
            patch.gameRating.dailyRatings[day].push({
              judge:author,
              rating:newRating
            })
          }
          return patch
  }