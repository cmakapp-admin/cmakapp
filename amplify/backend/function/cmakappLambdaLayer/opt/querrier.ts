 import {
    APIGatewayProxyWithCognitoAuthorizerHandler,
    APIGatewayProxyWithCognitoAuthorizerEvent,
    APIGatewayProxyResult,
  } from "aws-lambda";
  import { CognitoIdentityServiceProvider, DynamoDB } from "aws-sdk";
  import { getCurrentUser, getCurrentAttendee } from "./server-auth";
  import { Attendee, LetterEvaluation,  User, Position } from './cmak-types';
  import { createCmakHandler, CmakAuthorizationError, CmakNotFoundError } from "./server-tools";
  import { v4 as uuid, v4 } from "uuid";

export class Querrier{
  docClient = new DynamoDB.DocumentClient();

queryAllEvaluations():Promise<Map<string,LetterEvaluation[]>>
{
  if (!process.env.STORAGE_CMAKDYNAMO_NAME) {
    throw new CmakNotFoundError("Table name not found");
  }
  const params={
    TableName : process.env.STORAGE_CMAKDYNAMO_NAME,
    FilterExpression : "attribute_exists(registrationInfo)"
  }
return this.scanFully(params)  
  .then(items=>{
    let ratings:Map<string, LetterEvaluation[]>;
    (items as any[] as Attendee[]).forEach(attendee =>{
      attendee.motivationLetterRatings.forEach( rating=>{
        this.addToMap(rating.author.email, rating, ratings)
      }
      )
    }
    )
    return ratings;
  }
  )
}

async resetPositions(ids:string[]):Promise<null>{
  if (!process.env.STORAGE_CMAKDYNAMO_NAME) {
    throw new CmakNotFoundError("Table name not found");
  }

    return Promise.all(ids.map(id =>
      {
        let query={
          TableName : process.env.STORAGE_CMAKDYNAMO_NAME,
          Key:{
            id:id
          },
          UpdateExpression: "REMOVE  #posAtt",
          ExpressionAttributeNames: {"#posAtt":"position"}
        }
        console.log(JSON.stringify(query));
        return this.docClient.update(query).promise().then(result=>console.log(`${id} reset`),fail=>console.error(`${id} reset failed ${fail}`));
      }
    )).then();
  
}

updatePositions(id:string, position:Partial<Attendee>):Promise<Attendee>{
  if (!process.env.STORAGE_CMAKDYNAMO_NAME) {
    throw new CmakNotFoundError("Table name not found");
  }
  
  let params:DynamoDB.DocumentClient.UpdateItemInput={
    TableName: process.env.STORAGE_CMAKDYNAMO_NAME!,
    Key: {
      id:id      
    },

    UpdateExpression: "SET  #posAtt = :posVal",
    ExpressionAttributeValues: {":posVal":position.position},
    ExpressionAttributeNames: {"#posAtt":"position"},
    
    ReturnValues:"ALL_NEW"
  };
  
  console.log(JSON.stringify(params));
  return this.docClient
    .update(params)
    .promise()
    .then((r) => r.Attributes as Attendee);

}

queryById(id:string):Promise<Attendee[]>
{
    if (!process.env.STORAGE_CMAKDYNAMO_NAME) {
      throw new CmakNotFoundError("Table name not found");
      }

      const query={
        TableName : process.env.STORAGE_CMAKDYNAMO_NAME,
        IndexName : "id",
        KeyConditionExpression : "#name = :value",
        ExpressionAttributeValues : {":value":id},
        ExpressionAttributeNames : {"#name":"id"}
      };

    return this.docClient.query(query).promise().then(r=>r.Items as Attendee[]);

}

queryAttendees():Promise<Attendee[]>
{
  if (!process.env.STORAGE_CMAKDYNAMO_NAME) {
    throw new CmakNotFoundError("Table name not found");
  }
  console.log("getting all attendees");
  const params ={TableName:process.env.STORAGE_CMAKDYNAMO_NAME}
  return this.scanFully(params)
    .then(items=>items as any[] as Attendee[]);
}

private async scanFully(params: DynamoDB.DocumentClient.ScanInput, previouslyRead?:DynamoDB.AttributeMap[], lastEvaluatedKey?:DynamoDB.Key):Promise<DynamoDB.AttributeMap[]>{
  if (!previouslyRead){
    previouslyRead=[]
  }
  let newParams:DynamoDB.DocumentClient.ScanInput;
  if(lastEvaluatedKey){
    newParams = {...params,ExclusiveStartKey:lastEvaluatedKey}
  }else{
    newParams = params
  }
  console.log(JSON.stringify(newParams))
  return this.docClient
  .scan(newParams)
  .promise().then(r=>{
    if(r.LastEvaluatedKey){
      return this.scanFully(params,[...previouslyRead,...r.Items],r.LastEvaluatedKey)
    }else{
      return [...previouslyRead,...r.Items]
    }
  }
  )
}

private addToMap(author:string, evaluation:LetterEvaluation, map:Map<string, LetterEvaluation[]>):void{
  if(map.has(author)){
    map.get(author).push(evaluation)
  }
  else{
    let evaluations:LetterEvaluation[];
    evaluations.push(evaluation);
    map.set(author, evaluations)
  }
}

patchAttendee(patch:Partial<Attendee>):Promise<Attendee>{
  /*
  function foo(abc: ABC) {
  for (const [k, v] of Object.entries(abc)) {
    k  // Type is string
    v  // Type is any
  }
}
  */

 if(!patch.id){
   throw new Error("missing attendee.id")
 }

const updateFields = Object.entries(patch).map(([k,v])=>k).filter(v=>v!=="id");

let params={
  TableName: process.env.STORAGE_CMAKDYNAMO_NAME!,
  Key: {
    id: patch.id      
  },

  UpdateExpression: "SET "+updateFields.map((x,i) => `#att${i} = :val${i}`).join(","),
  ExpressionAttributeValues: updateFields.reduce((dict,v,i)=>{dict[`:val${i}`]=patch[v]; return dict},{}),
  ExpressionAttributeNames: updateFields.reduce((dict,v,i)=>{dict[`#att${i}`]=v; return dict},{}),
  
  ReturnValues:"ALL_NEW"
}
console.log(JSON.stringify(params));
return this.docClient
  .update(params)
  .promise()
  .then((r) => r.Attributes as Attendee);
}

}
export const QuerrierIns = new Querrier();