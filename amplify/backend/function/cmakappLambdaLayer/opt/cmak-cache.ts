export class Cached<A> {
  value?: A;
  stored?: Date;
  activePromise?: Promise<A>;

  get(): Promise<A> {
    if (this.activePromise) {
      return this.activePromise;
    } else if (
      this.value &&
      ((this.stored?.getTime() ?? 0) > (Date.now() - this.ttlInSec * 1000))
    ) {
      console.log(`cache hit ${this.label}`)
      return new Promise((resolve) => resolve(this.value!));
    } else {
      console.log(`cache miss ${this.label}`)
      return (this.activePromise = this.supplier().then((a) => this.store(a)));
    }
  }

  private store(a: A): A {
    this.stored = new Date();
    this.value = a;
    this.activePromise = undefined;
    return a;
  }

  purge() {
    console.log(`cache purge ${this.label}`)
    this.value = undefined;
    this.stored = undefined;
  }

  preCache(a: A | Promise<A>) {
    if (typeof a["then"] === "function") {
      this.activePromise = (a as Promise<A>).then((a) => this.store(a));
    } else {
      this.store(a as A);
    }
  }

  constructor(
    public supplier: () => Promise<A>,
    public ttlInSec: number = 60,
    public label: string = ""
  ) {}
}