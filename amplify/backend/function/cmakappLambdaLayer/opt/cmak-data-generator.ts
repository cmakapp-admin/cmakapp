import { Attendee, User, ApplicableFilter, Authorization } from "./cmak-types";
import { v4 as uuid } from "uuid";
import { LoremIpsum } from "lorem-ipsum";
import { CmakDefinition } from "./cmak-2021";

// let uuid=require("uuid").v4;
// let LoremIpsum=require("lorem-ipsum").LoremIpsum
// let CmakDefinition=require("./cmak-2021")
// let cmakDefinition = new CmakDefinition()
// import {CmakDefinition} from "./cmak-2021"

// import {LoremIpsum} from "lorem-ipsum"

const maleNames = [
  "Jiří",
  "Jan",
  "Petr",
  "Josef",
  "Pavel",
  "Martin",
  "Jaroslav",
  "Tomáš",
  "Miroslav",
  "Zdeněk",
  "František",
  "Václav",
  "Michal",
  "Milan",
  "Karel",
  "Jakub",
  "Lukáš",
  "David",
  "Vladimír",
  "Ladislav",
  "Ondřej",
  "Roman",
  "Stanislav",
  "Marek",
  "Radek",
  "Daniel",
  "Antonín",
  "Vojtěch",
  "Filip",
  "Adam",
  "Miloslav",
  "Matěj",
  "Aleš",
  "Jaromír",
  "Libor",
  "Dominik",
  "Patrik",
  "Vlastimil",
  "Jindřich",
  "Miloš",
  "Oldřich",
  "Lubomír",
  "Rudolf",
  "Ivan",
  "Luboš",
  "Robert",
  "Štěpán",
  "Radim",
  "Richard",
  "Bohumil",
  "Matyáš",
  "Vít",
  "Ivo",
  "Rostislav",
  "Luděk",
  "Dušan",
  "Kamil",
  "Šimon",
  "Vladislav",
  "Zbyněk",
  "Bohuslav",
  "Michael",
  "Alois",
  "Viktor",
  "Štefan",
  "Vítězslav",
  "René",
  "Jozef",
  "Ján",
  "Kryštof",
  "Eduard",
  "Marcel",
  "Emil",
  "Dalibor",
  "Ludvík",
  "Radomír",
  "Tadeáš",
  "Otakar",
  "Vilém",
  "Samuel",
  "Bedřich",
  "Alexandr",
  "Denis",
  "Vratislav",
  "Leoš",
  "Radovan",
  "Břetislav",
  "Marian",
  "Přemysl",
  "Robin",
  "Erik",
];

const femaleNames = [
  "Marie",
  "Jana",
  "Eva",
  "Hana",
  "Anna",
  "Lenka",
  "Kateřina",
  "Věra",
  "Lucie",
  "Alena",
  "Petra",
  "Jaroslava",
  "Veronika",
  "Martina",
  "Jitka",
  "Tereza",
  "Ludmila",
  "Helena",
  "Michaela",
  "Zdeňka",
  "Ivana",
  "Jarmila",
  "Monika",
  "Zuzana",
  "Jiřina",
  "Markéta",
  "Eliška",
  "Marcela",
  "Barbora",
  "Dagmar",
  "Dana",
  "Kristýna",
  "Vlasta",
  "Božena",
  "Irena",
  "Miroslava",
  "Libuše",
  "Pavla",
  "Marta",
  "Adéla",
  "Andrea",
];

const surnames = [
  "Novák",
  "Svoboda",
  "Novotný",
  "Černý",
  "Dvořák",
  "Procházka",
  "Veselý",
  "Kučera",
  "Hájek",
  "Jelínek",
  "Pokorný",
  "Růžička",
  "Beneš",
  "Horák",
  "Marek",
  "Král",
  "Čermák",
  "Zeman",
  "Fiala",
];

const lorem = new LoremIpsum({
  sentencesPerParagraph: {
    max: 8,
    min: 4,
  },
  wordsPerSentence: {
    max: 16,
    min: 4,
  },
});
const modifiers = ["Pod", "Nad", "Nedo", "Za", "Ne"];
const cmakDefinition = new CmakDefinition();

export function randomIntBetween(min: number = 0, max: number = 1): number {
  return Math.floor(min + Math.random() * (max - min + 1));
}
export function randomFromArray<A>(array: A[]): A {
  return array[randomIntBetween(0,array.length-1)];
}

export function randomNFromArray<A>(n: number, array: A[]): A[] {
  if (n >= array.length) return array;

  const indexes: number[] = [];
  while (indexes.length < n) {
    const x = randomIntBetween(0,array.length-1);
    if (!indexes.includes(x)) {
      indexes.push(x);
    }
  }

  return indexes.map((i) => array[i]);
}

function randomName(): {
  name: string;
  first: string;
  last: string;
  female: boolean;
} {
  const female = Math.random() < 0.5;
  const name = female
    ? randomFromArray(femaleNames)
    : randomFromArray(maleNames);
  let surname = randomFromArray(surnames);
  if (Math.random() > 0.6) {
    surname = randomFromArray(modifiers) + surname.toLowerCase();
  }
  //   console.log(`${surname} ${female}`)
  if (female) {
    switch (surname.charAt(surname.length - 1)) {
      case "ý":
        surname = surname.substring(0, surname.length - 1) + "á";
        break;
      case "a":
        surname = surname.substring(0, surname.length - 1) + "ová";
        break;
      case "k":
        surname =
          surname.charAt(surname.length - 2) == "e"
            ? surname.substring(0, surname.length - 2) + "ková"
            : surname + "ová";
        break;
      default:
        surname = surname + "ová";
    }
  }
  return {
    name: `${name} ${surname}`,
    first: name,
    last: surname,
    female: female,
  };
}

function randomEmail(first: string, last: string): string {
  const emailDomain =
    Math.random() > 0.5
      ? "google.com"
      : Math.random() > 0.5
      ? "email.cz"
      : "centrum.cz";
  return `${first
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "").toLowerCase()}.${last
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "").toLowerCase()}@${emailDomain}`;
}

export function randomAttendee(
  filledPreferences: number = 0,
  filledMotivationLetter: number = 0
): Attendee {
  const name = randomName();
  const email = randomEmail(name.first, name.last);
  const candidate:boolean = femaleNames.includes(name.first)?true:false;

  const result: Attendee = {
    id: uuid(),
    uuid: uuid() as string,
    email: email,
    pic: name.female
      ? "https://www.w3schools.com/w3images/avatar5.png"
      : "https://www.w3schools.com/howto/img_avatar.png",
    name: name.name,
    backupRoleCandidate: candidate
  };

  if (Math.random() * 100 < filledMotivationLetter) {
    result.registrationInfo = result.registrationInfo ?? {};
    result.registrationInfo.motivationalLetter = {
      delta: {
        ops: [
          { insert: lorem.generateParagraphs(randomIntBetween(1,5)) },
        ],
      },
    };
  }

  if (Math.random() * 100 < filledPreferences) {
    result.registrationInfo = result.registrationInfo ?? {};
    const roles = randomNFromArray(3, cmakDefinition.positions);
    result.registrationInfo.primaryRoles = roles;
    result.registrationInfo.secondaryRoleFilters = [
      ...ApplicableFilter.extractMatchingFilters(roles[0]),
      ...ApplicableFilter.extractMatchingFilters(
        randomFromArray(cmakDefinition.positions)
      ),
    ];
  }

  return result;
}

export function randomEvaluations(
  attendees: Attendee[],
  minPerEvaluator: number = 0,
  maxPerEvaluator: number = 2
) {

  

  const evaluators = attendees.filter((a) =>
    a.authorizations?.includes(Authorization.EVALUATOR)
  );

  const motivators = attendees.filter(
    (a) => a.registrationInfo?.motivationalLetter
  );

  randomNFromArray(40,attendees).forEach(a=>a.authorizations=[...a.authorizations??[]as Authorization[],Authorization.ORGANIZER]);

  evaluators.forEach((e) => {
    const count = randomIntBetween(minPerEvaluator, maxPerEvaluator);
    randomNFromArray(count, motivators).forEach((m) => {
      m.motivationLetterRatings = m.motivationLetterRatings ?? [];
      m.motivationLetterRatings.push({
        author: e,
        creativity: randomIntBetween(1, 100),
        grammar: randomIntBetween(1, 100),
        motivation: randomIntBetween(1, 100),
        karasIndex: randomIntBetween(1, 100),
      });
    });
  });
}

