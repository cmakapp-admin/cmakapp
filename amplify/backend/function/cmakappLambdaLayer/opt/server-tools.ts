import { Attendee } from "./cmak-types";
import {
  APIGatewayProxyWithCognitoAuthorizerHandler,
  APIGatewayProxyWithCognitoAuthorizerEvent,
  APIGatewayProxyResult,
} from "aws-lambda";

import {S3,SES} from "aws-sdk"
import { SendEmailRequest } from 'aws-sdk/clients/ses';

import {CmakConfig, CmakConfigImpl} from "./cmak-config"

import { getCurrentAttendee } from "./server-auth";
import { Cached } from "./cmak-cache";

export {Cached}

export type CmakMethodHandlerDeclaration = (
  event: APIGatewayProxyWithCognitoAuthorizerEvent,
  currentUser: Attendee
) => Promise<any>;
export interface CmakHandlerDeclaration {
  GET?: CmakMethodHandlerDeclaration;
  PUT?: CmakMethodHandlerDeclaration;
  POST?: CmakMethodHandlerDeclaration;
  PATCH?: CmakMethodHandlerDeclaration;
}

export class CmakErrorResponse {    
    reason:string
  constructor(reason: string | Error) {
      if(reason instanceof Error){
        this.reason = `${reason.name} ${reason.message}`
      } else {
          this.reason = reason
      }
  }
}

interface HttpFriendlyError{
    statusCode:number
}
export class CmakAuthorizationError extends Error implements HttpFriendlyError{
    statusCode=401
    constructor(message:string){
        super(message)
    }
}

export class CmakNotFoundError extends Error implements HttpFriendlyError{
    statusCode=404
    constructor(message:string){
        super(message)
    }
}

export class CmakInvalidRequestError extends Error implements HttpFriendlyError{
    statusCode=400
    constructor(message:string){
        super(message)
    }
}

export function createCmakHandler(
  declaration: CmakHandlerDeclaration
): APIGatewayProxyWithCognitoAuthorizerHandler {
  return async (event: APIGatewayProxyWithCognitoAuthorizerEvent) => {
    const currentUser = getCurrentAttendee(event);

    const handler = declaration[event.httpMethod]as CmakMethodHandlerDeclaration
    let status: number = 200;
    let payload: any;
    if (!handler) {
      status = 405;
      payload = new CmakErrorResponse(
        "allowed methods:" + Object.keys(declaration).join(",")
      );
    } else {
      await currentUser
        .then((user) =>{ 
            if (!user){
                throw new CmakAuthorizationError("user not found")
            }
            console.log(`handling request for ${user.email}, querystring:${JSON.stringify(event.queryStringParameters)} body:${event.body}`)
            return handler(event, user)
        })            
        .then((result) => {
          payload = result
          status = 200 ;
        })
        .catch((reason) => {
          console.log("handling failed", reason)
          if (reason['statusCode']){
              status = (reason as HttpFriendlyError).statusCode
          } else {
              status = 500
          }          
          payload = new CmakErrorResponse(reason);
        });
    }
    console.log(`status:${status}`)
    const stringifiedPayload=JSON.stringify(payload)
    console.log(`payload:${stringifiedPayload}`)
    return {
      statusCode: status,
      headers: {
        "x-version": "2",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "GET,PUT,DELETE,POST,OPTIONS",
      },
      body: stringifiedPayload,
    };
  };
}


/** STORAGE_CMAKBUCKET_BUCKETNAME */

export class ServerConfigProvider {
  private s3 = new S3()
  private cachedCfg=new Cached<CmakConfig>(()=>{
    console.log(`loading config from ${process.env.STORAGE_CMAKBUCKET_BUCKETNAME}:config/config.json`)
    return this.s3.getObject({
      Bucket:process.env.STORAGE_CMAKBUCKET_BUCKETNAME??"",
      Key:"config/config.json"
    }).promise().then(resp=>{
      return JSON.parse(resp.Body?.toString()??"{}") as Partial<CmakConfig>
    }).catch(err=>{
      console.log("failed to load config patch from s3 and that's ok, keeping default",err)
      return {} as Partial<CmakConfig>
    }).then(patch=>new CmakConfigImpl(patch))
  }, 120)

  get():Promise<CmakConfig>{
    return this.cachedCfg.get()
  }
}
const ses = new SES()

export function sendEmail(to:string, subject:string, text:string, html?:string){
  return serverConfigProvider.get().then(config=>{
  if (config.emailsAllowed){
    var params:SendEmailRequest = {
      Destination: {
        ToAddresses: [to],
      },
      Message: {
        Body: {
          Text: { Data: text },
          Html: html?{Data: html}:undefined
        },

        Subject: { Data: subject },
      },
      Source: "registrace@americkykongres.cz",
    };
    return ses.sendEmail(params).promise()
  }
})
}

export const serverConfigProvider = new ServerConfigProvider()