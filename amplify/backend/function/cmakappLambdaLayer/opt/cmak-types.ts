export interface User {
  name: string;
  email: string;
  pic: string;
}

export interface Accommodation {
  dorm: Dorm;
  room: number;
}

export interface Address {
  line1: string;
  city: string;
  zip: number;
}

export enum Authorization {
  ADMIN = "Admin",
  ORGANIZER = "Organizátor",
  EVALUATOR = "Hodnotitel",
  ATTENDEE = "Účastník"
}
export enum Sex {
  FEMALE = "Žena",
  MALE = "Muž"
}

export interface Person extends User {
  id: string
  uuid: string
  accommodation?: Accommodation
  services?: Services;
  financeInfo?: FinanceInfo;
  position?: Position;
  authorizations?: Authorization[];
  organizer?: OrganizerInfo;
  consent?: boolean;
  personalInfo?: PersonalInfo;
}

export interface PersonalInfo {
  phone: number;
  sex: Sex;
  address: Address;
}

export interface Services {
  accommodationInfo?: AccommodationInfo;
  lunch?: boolean;
  specialFoodNeeds?: string;
  transport?: boolean;
}

export interface FinanceInfo {
  fees: Fee[];
  paid: number;
  varSymbol: number;
  balanceCorrection?: number;
  balanceCorrectionComment?: string;
}

export interface AccommodationInfo {
  idNumber: number;
  nights: number;
  birthday: Date;
  dormPreference?: Dorm;
  roommatePreference?: string[];
  cellmatePreference?: string[];
}

export interface PriceList {
  night: number;
  lunch: number;
  transport: number;
  participation: number;
}

export interface Fee {
  name: string;
  price: number;
}

export enum Dorm {
  Bolevecka = 'Bolevecká',
  Heyrovskeho = 'Heyrovského'
}

export interface OrganizerInfo {
  orgTeam: OrgTeam[];
  teamHead?: OrgTeam;
  branchHead?: string;
  orgPosition?: OrgPosition;
}

export interface OrgTeam {
  name: string;
  branch: string;
}

export interface PositionFilter {
  type: 'committee' | 'party' | 'fraction' | 'body' | 'state' | 'lobbyType';
  value: string;
}

export interface PersonFilter {
  name: string;
  positionFilters: PositionFilter[];
  authorizations: Authorization[];

}

export interface DailyRating {
  judge: Person;
  rating: Rating;
}

export interface Rating {
  overall: number;
  type: "TCK" | "Congress" | "Lobby" | "Government";
}

export interface LobbyRating extends Rating {
  type: "Lobby";
  lobbing: number;
  readiness: number;
  mediaImage: number;
}

export interface CongressRating extends Rating {
  type: "Congress";
  activity: number;
  readiness: number;
  authenticity: number;
  teamwork: number;
}

export interface TckRating extends Rating {
  type: "TCK";
  creativity: number;
  initiative: number;
  integrity: number;
}

export interface GovernmentRating extends Rating {
  type: "Government";
  activity: number,
  readiness: number;
  teamwork: number;
}

export interface GameRating {
  dailyRatings: DailyRating[][];
  adjustment?: number;
  adjustmentAuthor?: Person;
}

export interface RatingResponsibilities {
  dailyRating: PositionFilter[];
  visibility: PositionFilter[];
  adjustment: PositionFilter[];
}

export interface Message {
  title: string;
  bodyMd: string;
  timeSent: Date;
  author: User;
  filter?: PositionFilter[][]; // ([0][0]&&[0][1]) || ([1][0])

}
// example
// [
//     [{type:"party",value:"Democrat"},{type:"committee",value:"VDR"}]
//     [{type:"party",value:"Republican"}
// ]

export enum Party {
  Republican = 'Republican', Democrat = 'Democrat'
}

export enum BodyType {
  Senate = 'Senate',
  Congress = 'Congress',
  Government = 'Government',
  Lobby = 'Lobby',
  TCK = 'TCK',
  Other = 'Other'
}

export enum SenateCommitteeId {
  VOS = 'VOS', VPZ = 'VPZ', VJ = 'VJ'
}

export enum CongressCommitteeId {
  VDR = 'VDR', VEH = 'VEH', VNB = 'VNB', VSP = 'VSP', VVVT = 'VVVT', VZV = 'VZV'
}

export enum State {
  Alabama = 'Alabama',
  Alaska = 'Aljaška',
  Arizona = 'Arizona',
  Arkansas = 'Arkansas',
  Colorado = 'Colorado',
  Connecticut = 'Connecticut',
  Delaware = 'Delaware',
  DistrictOfColumbia = 'District Of Columbia',
  Florida = 'Florida',
  Georgia = 'Georgia',
  Hawaii = 'Hawaj',
  Idaho = 'Idaho',
  Illinois = 'Illinois',
  Indiana = 'Indiana',
  Iowa = 'Iowa',
  California = 'Kalifornie',
  Kansas = 'Kansas',
  Kentucky = 'Kentucky',
  Lousiana = 'Lousiana',
  Maine = 'Maine',
  Maryland = 'Maryland',
  Massachusetts = 'Massachusetts',
  Michigan = 'Michigan',
  Minnesota = 'Minnesota',
  Mississippi = 'Mississippi',
  Missouri = 'Missouri',
  Montana = 'Montana',
  Nebraska = 'Nebraska',
  Nevada = 'Nevada',
  NewHampshire = 'New Hampshire',
  NewJersey = 'New Jersey',
  NewMexico = 'Nové Mexico',
  NewYork = 'New York',
  NorthernDakota = 'Severní Dakota',
  NorthernCarolina = 'Severní Carolina',
  Ohio = 'Ohio',
  Oklahoma = 'Oklahoma',
  Oregon = 'Oregon',
  Pennsylvania = 'Pennsylvánie',
  RhodeIsland = 'Rhode Island',
  SouthernCarolina = 'Jižní Karolína',
  SouthernDakota = 'Jižní Dakota',
  Tennessee = 'Tennessee',
  Texas = 'Texas',
  Utah = 'Utah',
  Vermont = 'Vermont',
  Virginia = 'Virginie',
  Washington = 'Washington',
  Wisconsin = 'Wisconsin',
  Wyoming = 'Wyoming',
  WesternVirginia = 'Západní Virginie'
}

export interface Fraction {
  id: string;
  name: string;
  party: Party;
}

export interface Position {
  body: BodyType;
  identifier: string;
}

export enum LobbyType {
  Np = 'Non-profit',
  Corp = 'Corporation',
  Un = 'Union'
}

export interface Organization {
  id: string;
  type: LobbyType;
  name: string;
}

export interface MediaOrganization {
  name: string;
}

export interface Department {
  id: string;
  name: string;
  availableCommittees: (CongressCommitteeId | SenateCommitteeId)[];
}

export interface OrgPosition extends Position {
  name: string; //Prezident Spojených států amerických etc
  party?: Party;
  fraction?: Fraction;
  committee?: SenateCommitteeId | CongressCommitteeId;
  state?: State;
  // flags: OrgPosFlag[];
}

/*
export enum OrgPosFlag {
  Observer = 'Observer',
  TCK = 'TCK',
  Investigator = 'Investigator',
  WhiteHouse = 'WhiteHouse',
  Capitol = 'Capitol',
  ThemeRole = 'ThemeRole',
  Lobby = 'Lobby',
  NonPlayable = 'NonPlayable',
  Senate = 'Senate',
  House = 'House',
  Photograph = 'Photograph',
  Secretariat = 'Secretariat'
}
// RIP HouseWhipp
*/

export interface PartisanPosition extends Position {
  party: Party;
}

export interface SenatePosition extends PartisanPosition {
  committee: SenateCommitteeId;
  state: State;
  fraction: Fraction;
}

export interface CongressPosition extends PartisanPosition {
  committee: CongressCommitteeId;
  state: State;
  fraction: Fraction;
}

export interface LobbyistPosition extends Position {
  organization: Organization;
  availableCommittees: CongressCommitteeId | SenateCommitteeId[];
}

export interface MinisterPosition extends PartisanPosition {
  department: Department;
  availableCommittees: (CongressCommitteeId | SenateCommitteeId)[];
}

export interface TckPosition extends Position {
  mediaOrg?: MediaOrganization;
}

export interface AgendaPoint {
  dummy?: string; // todo
}

export interface Link {
  text: string;
  uri: string;
}

export interface Committee {
  id: CongressCommitteeId | SenateCommitteeId;
  name: string;
  senate: boolean;
  congress: boolean;
  agenda: AgendaPoint[];
  materials: Link[];
  chairPersons: User[];
  attendees: User[];
}

export interface RoleBundle {
  name: string;
  role: Position;
}

export interface Letter {
  delta: any // TODO -> delta
}

export interface RegistrationInfo {
  primaryRoles?: Position[];// todo
  secondaryRoleFilters?: PositionFilter[];// todo
  motivationalLetter?: Letter;
}

export interface LetterEvaluation {
  grammar: number;
  creativity: number; // The most important category - secondary criteria
  motivation: number;
  karasIndex: number;
  author?: User; // for automatically computed evals, the author is not compulsory
  correctedBy?: User; //admin only if needed
}

export interface FinalLetterRating {
  grammar: number;
  creativity: number; // The most important category - secondary criteria
  motivation: number;
  karasIndex: number;
  total: number;
}

export interface Attendee extends Person {
  // ...
  rejected?: boolean;
  backupRoleCandidate?: boolean;
  registrationInfo?: RegistrationInfo;
  motivationLetterRatings?: LetterEvaluation[];
  finalLetterRating?: FinalLetterRating;
  gameRating?: GameRating;
}


/*

body : lobby
committee:x
state Cali
state NY
party
org:  media/lobby

*/

export abstract class ApplicableFilter {
  value: string;
  abstract matches(position: Position): boolean;
  constructor(value: string) {
    this.value = value
  };

  static makeApplicable(filters: PositionFilter[]): ApplicableFilter[] {
    return filters.map(f => {
      switch (f.type) {
        case 'committee':
          return new CommitteeFilter(f)
        case 'party':
          return new PartyFilter(f)
        case 'fraction':
          return new FractionFilter(f)
        case 'body':
          return new BodyFilter(f)
        case 'state':
          return new StateFilter(f)
        case 'lobbyType':
          return new LobbyTypeFilter(f)
      }
    })
  }

  static extractMatchingFilters(position: Position): PositionFilter[] {
    return [
      ...BodyFilter.extract(position),
      ...CommitteeFilter.extract(position),
      ...PartyFilter.extract(position),
      ...FractionFilter.extract(position),
      ...LobbyTypeFilter.extract(position)
    ]
  }
}

export class BodyFilter extends ApplicableFilter implements PositionFilter {
  type: 'body' = 'body'

  matches(position: Position) {
    const bodyType: BodyType = this.value as BodyType
    return bodyType === position.body
  }

  static extract(position: Position): PositionFilter[] {
    return [{ type: 'body', value: position.body }]
  }

  constructor(filter: PositionFilter | { value: string }) {
    super(filter.value)
    if ('type' in filter && filter.type != this.type) {
      throw new Error('incorrect filter type (' + filter.type + ')')
    }
  }
}
export class CommitteeFilter extends ApplicableFilter implements PositionFilter {
  type: 'committee' = 'committee'

  matches(position: Position) {
    const committeeId: CongressCommitteeId | SenateCommitteeId = this.value as (CongressCommitteeId | SenateCommitteeId)
    switch (position.body) {
      case BodyType.Senate:
      case BodyType.Congress: {
        const committeeIdToMatch = (position as (SenatePosition | CongressPosition | OrgPosition)).committee
        return committeeId === committeeIdToMatch
      }

      case BodyType.Government: {
        const govPosition = position as MinisterPosition
        return govPosition.availableCommittees.includes(committeeId)
      }
      default:
        return false
    }
  }

  static extract(position: Position): PositionFilter[] {
    switch (position.body) {
      case BodyType.Senate:
      case BodyType.Congress:
        return [{
          type: 'committee',
          value: (position as (SenatePosition | CongressPosition)).committee
        }]

      case BodyType.Government: {
        const govPosition = position as MinisterPosition
        return govPosition.availableCommittees.map(c => { return { type: 'committee', value: c } })
      }
      default:
        return []
    }
  }

  constructor(filter: PositionFilter | { value: string }) {
    super(filter.value)
    if ('type' in filter && filter.type != this.type) {
      throw new Error('incorrect filter type')
    }
  }
}

export class PartyFilter extends ApplicableFilter implements PositionFilter {
  type: 'party' = 'party';

  matches(position: Position): boolean {
    const party = (position as PartisanPosition | OrgPosition).party
    return party === this.value
  }

  static extract(position: Position): PositionFilter[] {
    switch (position.body) {
      case BodyType.Senate:
      case BodyType.Congress: {
        return [{ type: 'party', value: (position as PartisanPosition | OrgPosition).party! }]
      }
      default:
        return []
    }
  }

  constructor(filter: PositionFilter | { value: string }) {
    super(filter.value)
    if ('type' in filter && filter.type != this.type) {
      throw new Error('incorrect filter type')
    }
  }
}

export class FractionFilter extends ApplicableFilter implements PositionFilter {
  type: 'fraction' = 'fraction';
  value: string;

  matches(position: Position): boolean {
    const fractionId = (position as CongressPosition | SenatePosition).fraction?.id
    return fractionId === this.value
  }

  static extract(position: Position): PositionFilter[] {
    switch (position.body) {
      case BodyType.Congress: {
        return [{ type: 'fraction', value: (position as CongressPosition).fraction.id }]
      }
      case BodyType.Senate: {
        return [{ type: 'fraction', value: (position as SenatePosition).fraction.id }]
      }
      default:
        return []
    }
  }

  constructor(filter: PositionFilter) {
    super(filter.value)
    if (filter.type != this.type) {
      throw new Error('incorrect filter type')
    }
    this.value = filter.value
  }
}

export class StateFilter extends ApplicableFilter implements PositionFilter {
  type: 'state' = 'state';

  matches(position: Position): boolean {
    const state = (position as SenatePosition | CongressPosition).state
    return state === this.value
  }

  constructor(filter: PositionFilter | { value: string }) {
    super(filter.value)
    if ('type' in filter && filter.type != this.type) {
      throw new Error('incorrect filter type')
    }
  }
}

export class LobbyTypeFilter extends ApplicableFilter implements PositionFilter {
  type: 'lobbyType' = 'lobbyType';

  static extract(position: Position): PositionFilter[] {
    switch (position.body) {
      case BodyType.Lobby: {
        return [{ type: 'lobbyType', value: (position as LobbyistPosition).organization.type }]
      }
      default:
        return []
    }
  }

  matches(position: Position): boolean {
    const lobbyType = (position as LobbyistPosition).organization?.type
    return lobbyType === this.value
  }

  constructor(filter: PositionFilter | { value: string }) {
    super(filter.value)
    if ('type' in filter && filter.type != this.type) {
      throw new Error('incorrect filter type')
    }
  }
}

export function positionToText(positionToConvert: Position, committeesById: Map<string, Committee>): string {
  switch (positionToConvert.body) {
    case BodyType.Senate: {
      let party: string;
      const position = positionToConvert as SenatePosition;
      if (position.party === "Republican") {
        party = "Republikány";
      } else {
        party = "Demokraty";
      }
      return `Senátor za ${party}/${position.fraction?.name}, ${position.state} ve ${committeesById.get(position.committee)?.name.replace("Výbor", "Výboru")}`
    }
    case BodyType.Congress: {
      const position = positionToConvert as CongressPosition;
      let party: string;
      if (position.party === "Republican") {
        party = "Republikány";
      } else {
        party = "Demokraty";
      }
      return `Poslanec za ${party}/${position.fraction?.name}, ${position.state} ve ${committeesById.get(position.committee)?.name.replace("Výbor", "Výboru")} `;
    }
    case BodyType.Government: {
      const position = positionToConvert as MinisterPosition;
      return "Ministr: " + position.department.name;
    }

    case BodyType.Lobby: {
      const position = positionToConvert as LobbyistPosition;
      return `Lobista za ${position.organization.name}`;
    }

    case BodyType.TCK: {
      return "Novinář ";
    }
    default:
      return "Neznámá pozice";
  }

}

export function feeGenerator(services: Services | undefined, prices: PriceList): Fee[] {
  const fees: Fee[] = []

  if (services?.lunch) {
    fees.push({ name: "Obědy", price: prices.lunch })
  }
  if (services?.transport) {
    fees.push({ name: "Celotýdenní jízdenka na MHD v Plzni", price: prices.transport })
  }
  if (services?.accommodationInfo?.nights) {
    fees.push({ name: services.accommodationInfo.nights + " nocí na kolejích UK", price: (prices.night) * services.accommodationInfo.nights })
  }
  fees.push({ name: "Účastnický poplatek", price: prices.participation })
  return fees;
}

export function demandEstimate(attendees: Attendee[], roles?: Position[]): [string, number][] {
  console.log("calculating demand of " + attendees.length + " attendees")
  const roleAvailability = roles?.reduce((map, p) => { map.set(p.identifier, (map.get(p.identifier) ?? 0) + 1); return map }, new Map<string, number>()) ?? new Map<string, number>()
  const result = new Map<string, number>();
  attendees.forEach(a => {
    (a.registrationInfo?.primaryRoles ?? ([] as Position[])).forEach(p => {
      if (p) {
        result.set(p.identifier, (result.get(p.identifier) ?? 0) + 1 / (roleAvailability.get(p.identifier) ?? 1))
      }
    })
  })

  return Array.from(result.entries());
}

export function generateVarSymbol(id: string, existingVarSymbols: number[]): number {
  let result = 0
  for (let i = 0; i < id.length; i++) {
    result = (result * 13 + id.charCodeAt(i)) % 1000000
  }
  const prefix = new Date().getFullYear() * 1000000
  while (existingVarSymbols.includes(prefix + result)) {
    result = (result + 1) % 1000000
  }
  return prefix + result
}

/**
 * grants attendee authorization and initializes finance info for those that have position assigned and do not have any authorization yet
 * @param attendees 
 * @param pricelist 
 * @returns 
 */
export function grantAttendees(attendees: Attendee[], pricelist: PriceList): { accepted: Attendee[], rejected: Attendee[] } {
  const result = {
    accepted: [] as Attendee[],
    rejected: [] as Attendee[]
  }
  const symbols: number[] = attendees.map(a => a.financeInfo?.varSymbol ?? 0).filter(x => x > 0)
  attendees.forEach(attendee => {
    if (attendee.authorizations?.length) {
      return //any authorization means we do not want to continue processing
    }
    if (attendee.position) {
      //newly accepted
      if (!attendee.financeInfo) {
        const varSymbol = generateVarSymbol(attendee.id, symbols)
        symbols.push(varSymbol)
        attendee.financeInfo = {
          fees: feeGenerator(undefined, pricelist),
          paid: 0,
          varSymbol: varSymbol
        }
      }
      attendee.rejected = false
      attendee.authorizations = [Authorization.ATTENDEE]
      result.accepted.push(attendee)
    } else {
      if (!attendee.rejected) {
        //newly rejected
        attendee.rejected = true
        result.rejected.push(attendee)
      }
    }
  })

  return result
}

export function validatePositionDistribution(map: { [key: string]: Position }, positions: Position[]): boolean {
  const remainingPositions = [...positions]
  try {
    Object.entries(map).forEach(([userId, position]) => {
      const index = remainingPositions.findIndex(p => p.identifier == position.identifier)
      if (index < 0) {
        throw "failed"
      } else {
        remainingPositions.splice(index, 1)
      }
    })
  } catch (e) {
    return false
  }

  return true
}

export function isWithinFilters(attendee: Attendee, positionFilters: ApplicableFilter[]):boolean {
  if(positionFilters.find((singleFilter) => 
    attendee.position ? singleFilter.matches(attendee.position) : false
  )){
    return true
  }
  else{
    return false
  }
}

export function getRatingCategories(position: Position): Array<{label: string, key: string}> {
  const type = getRatingType(position) 
  switch (type) {
    case "Lobby": {
      return [
        {key:"lobbing", label: "Lobbing"},
        {key:"readiness", label: "Připravenost"},
        {key:"mediaImage", label: "Mediální obraz"}
      ]
    }
    case "Congress": {
      return [
        {key:"activity", label: "Aktivita"},
        {key:"readiness", label: "Připravenost"},
        {key:"authenticity", label: "Autentičnost"},
        {key:"teamwork", label: "Týmová práce"}
      ]
    }
    case "TCK": {
      return [
        {key:"creativity", label: "Kreativita"},
        {key:"initiative", label: "Iniciativa"},
        {key:"integrity", label: "Integrita"},
      ]
    }
    case "Government": {
      return [
        {key:"activity", label: "Aktivita"},
        {key:"readiness", label: "Připravenost"},
        {key:"teamwork", label: "Týmová práce"}
      ]
    }
    default:
        return []
  }
}

export function calculateSingleRatingOverall(person: Attendee, rating: Rating): number {
  return getRatingCategories(person.position!).reduce((avg, curr, index, categories) => 
     avg + (rating[curr.key])/categories.length
  , 0);
}

export function getAsPerson(attendee:Attendee):Person{
  const person:Person ={
    name:attendee.name,
    pic:attendee.pic,
    email:attendee.email,
    id: attendee.id,
    uuid: attendee.uuid
  }
  return person
}

export function getRatingType(position: Position): "TCK" | "Congress" | "Lobby" | "Government"| null {
  switch (position.body) {
    case BodyType.Congress:
    case BodyType.Senate: 
      return "Congress"
    case BodyType.TCK:
      return "TCK"
    case BodyType.Government:
      return "Government"
    case BodyType.Lobby:
      return "Lobby"
    case BodyType.Other:
      return null
  }
}