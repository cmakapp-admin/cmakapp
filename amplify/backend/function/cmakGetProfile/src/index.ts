/* Amplify Params - DO NOT EDIT
	AUTH_CMAKAPPF72350E2_USERPOOLID
	ENV
	REGION
	STORAGE_CMAKDYNAMO_ARN
	STORAGE_CMAKDYNAMO_NAME
Amplify Params - DO NOT EDIT */

import {
  APIGatewayProxyWithCognitoAuthorizerHandler,
  APIGatewayProxyWithCognitoAuthorizerEvent,
  APIGatewayProxyResult,
} from "aws-lambda";
import { CognitoIdentityServiceProvider, DynamoDB } from "aws-sdk";
import { getCurrentUser, getCurrentAttendee, getUserId, filterAtts, getAttendeeById } from "/opt/server-auth";
import { Attendee, Authorization, FinanceInfo, feeGenerator, PriceList, generateVarSymbol } from "/opt/cmak-types";
import { CmakDefinition } from "/opt/cmak-2021";
import { v4 as uuid, v4 } from "uuid";
import { QuerrierIns } from "/opt/querrier";
import { config } from "/opt/cmak-config";
import { createCmakHandler, CmakAuthorizationError, CmakNotFoundError, serverConfigProvider, CmakInvalidRequestError } from "/opt/server-tools";

const docClient = new DynamoDB.DocumentClient();
const theCmak = new CmakDefinition()

export const handler = createCmakHandler({
  GET: (event, currentUser) => getProfile(event),
  PATCH: (event, currentUser) => updateProfile(event, currentUser)
})

async function getProfile(
  event: APIGatewayProxyWithCognitoAuthorizerEvent
): Promise<Attendee> {
  if (!process.env.STORAGE_CMAKDYNAMO_NAME) {
    throw new CmakNotFoundError("Table name not found");
  } 
  return getCurrentAttendee(event);
}

async function updateProfile(
  event: APIGatewayProxyWithCognitoAuthorizerEvent,
  user : Attendee
): Promise<Attendee> {  

  const update: Partial<Attendee> = JSON.parse(event.body??'{}');
  let updatedAttendee:Attendee;
  if (event.queryStringParameters?.id){
    if (event.queryStringParameters?.id==user.id){
      updatedAttendee = user
    } else if(user.authorizations?.includes(Authorization.ADMIN)){
      updatedAttendee = await getAttendeeById(event.queryStringParameters.id);
    } else {
      throw new CmakAuthorizationError("Insufficient permissions");
    }
  }
  else{
    updatedAttendee = user;
  }

  if (!(updatedAttendee.consent || update.consent || user.authorizations?.includes(Authorization.ADMIN))) {

    throw new CmakAuthorizationError("Missing consent");
  }
  const patch:Partial<Attendee>={id:updatedAttendee.id}
  if (update.consent) {
    patch.consent=update.consent;
  }

  if (update.registrationInfo) {
    if(!(await serverConfigProvider.get()).registrationsAllowed){
      throw new CmakAuthorizationError("Registrace uzamčeny");
    }
    patch.registrationInfo=update.registrationInfo
  }

  if (update.personalInfo) {
    patch.personalInfo=update.personalInfo
  }

  if (update.name) {
    patch.name=update.name
  }

  if (update.backupRoleCandidate!=undefined){
    patch.backupRoleCandidate=update.backupRoleCandidate
  }

  if (update.financeInfo){
    if(user.authorizations?.includes(Authorization.ADMIN)){
      if (!updatedAttendee.financeInfo){
        throw new CmakInvalidRequestError("Uživatel nemá založeno platební info");
      }
      patch.financeInfo = updatedAttendee.financeInfo
      patch.financeInfo.balanceCorrection = update.financeInfo.balanceCorrection
      patch.financeInfo.balanceCorrectionComment = update.financeInfo.balanceCorrectionComment
    }else{
      throw new CmakAuthorizationError("Insufficient permissions to edit finance info");
    }
  }

  if (update.services){
    let usedPriceList : PriceList
    if(updatedAttendee.authorizations.includes(Authorization.ORGANIZER)){
      usedPriceList = theCmak.orgPriceList
    }
    else if(updatedAttendee.authorizations.includes(Authorization.ATTENDEE)){
      usedPriceList = theCmak.priceList
    }
    else{
      throw new CmakAuthorizationError("User is not elligible for services")
    }
    let newFees = feeGenerator(update.services, usedPriceList)
    if(!updatedAttendee.financeInfo){
      let symbols = new Array<number>()
      ;(await QuerrierIns.queryAttendees()).forEach(element => {
        if(element.financeInfo){
          symbols.push(element.financeInfo.varSymbol)
        }
      });
      patch.financeInfo = {
        varSymbol : generateVarSymbol(updatedAttendee.id,symbols),
        fees:newFees,
        paid:0
      }
    }
    else{
      updatedAttendee.financeInfo.fees=newFees
      patch.financeInfo = updatedAttendee.financeInfo
    }       
    patch.services=update.services
  }


  if (update.authorizations && user.authorizations?.includes(Authorization.ADMIN)) {
    patch.authorizations=update.authorizations;
  }

  
  if (update.organizer && user.authorizations?.includes(Authorization.ADMIN)) {
    patch.organizer=update.organizer
  }
  
  return QuerrierIns.patchAttendee(patch)
}


