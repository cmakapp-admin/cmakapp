/* Amplify Params - DO NOT EDIT
  AUTH_CMAKAPPF72350E2_USERPOOLID
  ENV
  REGION
  STORAGE_CMAKBUCKET_BUCKETNAME
  STORAGE_CMAKDYNAMO_ARN
  STORAGE_CMAKDYNAMO_NAME
Amplify Params - DO NOT EDIT */
import {
  APIGatewayProxyWithCognitoAuthorizerHandler,
  APIGatewayProxyWithCognitoAuthorizerEvent,
  APIGatewayProxyResult,
} from "aws-lambda";
import { CognitoIdentityServiceProvider, DynamoDB } from 'aws-sdk';
import { getCurrentUser, getCurrentAttendee, getUserId, filterAtts, getAttendeeById } from "/opt/server-auth";
import { Attendee, Authorization, FinanceInfo, feeGenerator, RatingResponsibilities, Position, PositionFilter, 
  ApplicableFilter, Rating, GameRating, DailyRating, isWithinFilters, User, Person, getAsPerson } from "/opt/cmak-types";
import { CmakDefinition, determineRatingResponsibility } from "/opt/cmak-2021";
import { v4 as uuid, v4 } from "uuid";
import { QuerrierIns } from "/opt/querrier";
import { getValidatedDailyRatingPatch, getValidatedGameRating } from "/opt/cmak-core";
import { config } from "/opt/cmak-config";
import { createCmakHandler, CmakAuthorizationError, CmakNotFoundError, serverConfigProvider, CmakInvalidRequestError } from "/opt/server-tools";

const docClient = new DynamoDB.DocumentClient();

export const handler = createCmakHandler({
  GET: (event, currentUser) => getAttendeesToRate(currentUser),
  POST: (event, currentUser) => newChangeInRating(event, currentUser)
})

// TODO Optimize functions

async function getAttendeesToRate(currentUser: Attendee): Promise<Array<Attendee> | undefined> {
  if (currentUser.authorizations?.includes(Authorization.ORGANIZER)) {
    const responsibilities = determineRatingResponsibility(currentUser!)
    if (responsibilities) {
      let positionFilters: ApplicableFilter[] = ApplicableFilter.makeApplicable([...responsibilities.adjustment, ...responsibilities.dailyRating, ...responsibilities.visibility])     
      return QuerrierIns.queryAttendees().then(attendees => {
        return attendees.filter((attendee) => {
          return isWithinFilters(attendee, positionFilters)
        });
      })
      
    }
  } else {
    throw new CmakAuthorizationError("Insufficient permisions");
  }
}

async function newChangeInRating(event:APIGatewayProxyWithCognitoAuthorizerEvent, currentUser:Attendee):Promise<Attendee>{
  if(event.queryStringParameters){
    if(event.body && event.queryStringParameters.id && event.queryStringParameters.day){
      return await setDailyRating(JSON.parse(event.body) as Rating, currentUser, event.queryStringParameters.id, Number(event.queryStringParameters.day))
    }
    else if(event.queryStringParameters.id && event.queryStringParameters.adjustment){
      return await setAdjustment(currentUser, event.queryStringParameters.id, Number(event.queryStringParameters.adjustment))
    } 
    else{
      throw new CmakInvalidRequestError("Request didn't contain necessary parameters")
    }     
  }
  else{
    throw new CmakInvalidRequestError("Request didn't contain necessary parameters")
  }
}

async function setDailyRating(rating: Rating, currentUser: Attendee, targetAttendeeId:string, ratingDay:number):Promise<Attendee> {
  if (!process.env.STORAGE_CMAKDYNAMO_NAME) {
    throw new CmakNotFoundError("Table name not found");
  }
  if (currentUser.authorizations?.includes(Authorization.ORGANIZER)) {
    const responsibilities = determineRatingResponsibility(currentUser)
    if (responsibilities) {
      const attendee = await getAttendeeById(targetAttendeeId)
      const positionFilters: ApplicableFilter[] = ApplicableFilter.makeApplicable(responsibilities.dailyRating)
      if (attendee && isWithinFilters(attendee,positionFilters)) {      
        return await QuerrierIns.patchAttendee(getValidatedDailyRatingPatch(attendee.id, getValidatedGameRating(attendee), getAsPerson(currentUser), ratingDay, rating))
      }
      else{
        throw new CmakAuthorizationError("Insufficient rating responsibilities or invalid rating target")
      }
    } else {
      throw new CmakAuthorizationError("Insufficient rating responsibilities")
    }
  } else {
    throw new CmakAuthorizationError("Insufficient permisions");
  }
}

async function setAdjustment(currentUser: Attendee, adjustedAttendeeId:string, adjustment:number):Promise<Attendee> {
  if (!process.env.STORAGE_CMAKDYNAMO_NAME) {
    throw new CmakNotFoundError("Table name not found");
  }
  if (currentUser.authorizations?.includes(Authorization.ORGANIZER)) {
    const responsibilities = determineRatingResponsibility(currentUser)
    if (responsibilities) {
      const attendee = await getAttendeeById(adjustedAttendeeId)
      const filters: ApplicableFilter[] = ApplicableFilter.makeApplicable(responsibilities.dailyRating)
      if (attendee && isWithinFilters(attendee,filters)) {

        let patch: Partial<Attendee> = { 
          id: attendee.id,
          gameRating: getValidatedGameRating(attendee)
         }
        patch.gameRating!.adjustment = adjustment
        patch.gameRating!.adjustmentAuthor = getAsPerson(currentUser)
        return await QuerrierIns.patchAttendee(patch)
      }
      else{
        throw new CmakAuthorizationError("Insufficient rating responsibilities or target attendee not found");
      }
    }
    else{
      throw new CmakAuthorizationError("Insufficient rating responsibilities");
    }
  }
  else{
    throw new CmakAuthorizationError("Insufficient authorization");
  }
}
