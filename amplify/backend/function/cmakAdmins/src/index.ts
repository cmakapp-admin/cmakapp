/* Amplify Params - DO NOT EDIT
	AUTH_CMAKAPPF72350E2_USERPOOLID
	ENV
	REGION
	STORAGE_CMAKDYNAMO_ARN
	STORAGE_CMAKDYNAMO_NAME
Amplify Params - DO NOT EDIT */

import {
    APIGatewayProxyWithCognitoAuthorizerHandler,
    APIGatewayProxyWithCognitoAuthorizerEvent,
    APIGatewayProxyResult,
  } from "aws-lambda";
  import { CognitoIdentityServiceProvider, DynamoDB } from "aws-sdk";
  import { getCurrentUser, getCurrentAttendee, getUserId, getAttendeeById } from "/opt/server-auth";
  import { Attendee, Authorization, LetterEvaluation, Position, grantAttendees, validatePositionDistribution } from "/opt/cmak-types";
  import {CmakDefinition} from "/opt/cmak-2021";
  import { createCmakHandler, CmakAuthorizationError, CmakNotFoundError, sendEmail, CmakInvalidRequestError } from "/opt/server-tools";
  import { v4 as uuid, v4 } from "uuid";
  import { QuerrierIns } from "/opt/querrier";

  const docClient = new DynamoDB.DocumentClient();
  const theCmak = new CmakDefinition();
  export const handler = createCmakHandler({
    GET: (event, currentUser) => getProfiles(event, currentUser),
    PATCH: (event, currentUser) => assignPositions(event, currentUser),
    POST:(event,currentUser) => adminPost(event,currentUser)
  })

async function getProfiles(
    event: APIGatewayProxyWithCognitoAuthorizerEvent, currentUser:Attendee
  ): Promise<Attendee[]> {
    if (!process.env.STORAGE_CMAKDYNAMO_NAME) {
      throw new CmakNotFoundError("Table name not found");
    }       
    if(currentUser.authorizations?.includes(Authorization.ADMIN)){
      return QuerrierIns.queryAttendees()
      }else{
          throw new CmakAuthorizationError("Insufficient permisions");
      }
  }

  async function adminPost(
    event: APIGatewayProxyWithCognitoAuthorizerEvent, currentUser:Attendee
  ): Promise<void> {
    if (!process.env.STORAGE_CMAKDYNAMO_NAME) {
      throw new CmakNotFoundError("Table name not found");
    }       
    if(currentUser.authorizations?.includes(Authorization.ADMIN)){      
      if(event.queryStringParameters["mode"]==="grantAttendees"){
        return QuerrierIns.queryAttendees().then(attendees=>{
          const grantResult = grantAttendees(attendees,theCmak.priceList)
          const futures:Promise<void>[]=[]
          for(const attendee of grantResult.accepted) {
            console.log(`granting attendee status to ${attendee.name}`)
            const patch:Partial<Attendee>={id:attendee.id}
            patch.authorizations=attendee.authorizations
            patch.financeInfo=attendee.financeInfo
            patch.rejected=false
            futures.push(QuerrierIns.patchAttendee(patch)
            .then(a=>{sendEmail(a.email,"ČMAK 2021 – Přivítání do Kongresu",theCmak.goodNewsEmailText(a.position), theCmak.goodNewsEmailHtml(a.position))})
            )
          }
          for(const attendee of grantResult.rejected) {
            console.log(`rejecting attendee ${attendee.name}`)
            const patch:Partial<Attendee>={id:attendee.id}
            patch.authorizations=[]          
            patch.rejected=true
            futures.push(QuerrierIns.patchAttendee(patch)
            .then(a=>{sendEmail(a.email,"ČMAK 2021 – Smutná zpráva",theCmak.badNewsEmailText(), theCmak.badNewsEmailHtml())})
            )
          }
          return Promise.all(futures).then(a=>{})
        });
      }else if (event.queryStringParameters["mode"]==="updatePayments"){
        //expecting list of tuples - [user id, total amount paid]
        const payments:[string,number][]=JSON.parse(event.body??'[]')

        return Promise.all(payments.map(([id,amount])=>{
          return getAttendeeById(id)
          .then(a=>{
            const newfinance=a.financeInfo!;
            newfinance.paid=amount
            QuerrierIns.patchAttendee({id:a.id,financeInfo:newfinance})
          })
        })).then(a=>{})
      }
    }else{
          throw new CmakAuthorizationError("Insufficient permisions");
    }
  }

async function assignPositions(event: APIGatewayProxyWithCognitoAuthorizerEvent, currentUser: Attendee): Promise<void>{
  if (!process.env.STORAGE_CMAKDYNAMO_NAME) {
    throw new CmakNotFoundError("Table name not found");
  }
  let map:{[key:string]:Position} = JSON.parse(event.body??'[]');
    if(!validatePositionDistribution(map,theCmak.positions)){
      throw new CmakInvalidRequestError("Neplatná distribuce rolí")
    }
    if (currentUser.authorizations){
      if(currentUser.authorizations.includes(Authorization.ADMIN)){
        await QuerrierIns.queryAttendees().then(attendees=>{
         let idsToSkip = Object.keys(map)
         let ids = attendees.map(a=>a.id).filter(id=>!idsToSkip.includes(id))
         return QuerrierIns.resetPositions(ids)
        });
        await Promise.all(Object.entries(map).map(([key, value]) => 
          QuerrierIns.updatePositions(key, {position:value} as Partial<Attendee>)
        ));
        return;
      }
    }
    throw new CmakAuthorizationError("Insufficient permisions");
}