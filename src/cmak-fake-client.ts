import {
  Person,
  Attendee,
  RegistrationInfo,
  Position,
  BodyType,
  LobbyistPosition,
  MinisterPosition,
  CongressPosition,
  SenatePosition,
  PositionFilter,
  LetterEvaluation,
  Letter,
  Authorization,
  demandEstimate,
  Address,
  OrganizerInfo,
  Services,
  FinanceInfo,
  feeGenerator,
  PersonalInfo,
  grantAttendees,
  validatePositionDistribution,
  RatingResponsibilities,
  Rating,
  ApplicableFilter,
  DailyRating,
  GameRating,
  isWithinFilters
} from '@/../amplify/backend/function/cmakappLambdaLayer/opt/cmak-types';
import {
  randomAttendee,
  randomEvaluations,
  randomFromArray,
  randomNFromArray,
} from '@/../amplify/backend/function/cmakappLambdaLayer/opt/cmak-data-generator';
import { CmakClient } from "./cmak-client";
import { theCmak, theStore } from './cmak-ui';
import { determineRatingResponsibility } from '@/../amplify/backend/function/cmakappLambdaLayer/opt/cmak-2021';

export class FakeCmakClient implements CmakClient {
  fake = true
  currentUser?: Attendee;
  evaluators: Attendee[] = [];
  admins: Attendee[] = [];
  attendees: Attendee[] = [];

  private delay<A>(f: () => A): Promise<A> {
    return new Promise((resolve, reject) =>
      setTimeout(() => {
        try {
          resolve(f());
        } catch (error) {
          reject(error)
        }
      }, 800)
    );
  }

  getMe(): Promise<Attendee> {
    return this.delay(() => this.currentUser!);
  }

  updateRegistration(registration: RegistrationInfo): Promise<Attendee> {
    return this.delay(() => {
      this.currentUser!.registrationInfo = registration;
      return this.currentUser!;
    });
  }

  updateServices(services: Services): Promise<Attendee> {
    return this.delay(() => {
      if (this.currentUser) {
        this.currentUser!.services = services;
        const financeInfo: FinanceInfo = this.currentUser!.financeInfo ?? { fees: [], paid: 0, varSymbol: Math.floor(Math.random() * 1000000) };
        financeInfo.fees = feeGenerator(services, theCmak.priceList)
        this.currentUser!.financeInfo = financeInfo
        return this.currentUser!;
      } else {
        throw new Error("not logged in")
      }
    });
  }

  updatePersonalInfo(pi: PersonalInfo): Promise<Attendee> {
    return this.delay(() => {
      this.currentUser!.personalInfo = pi;
      console.log("Personal info saved: ", pi)
      return this.currentUser!;
    });
  }

  updateName(name: string): Promise<Attendee> {
    return this.delay(() => {
      this.currentUser!.name = name;
      console.log("name saved: ", name)
      return this.currentUser!;
    });
  }

  // saveAddress(address: Address): Promise<Attendee> {
  //   return this.delay(() => {
  //     this.currentUser!.address = address;
  //     console.log("address saved: ",address)
  //     return this.currentUser!;
  //   });
  // }

  grantConsent(): Promise<Attendee> {
    return this.delay(() => {
      this.currentUser!.consent = true;
      console.log("consent saved: ", true)
      return this.currentUser!;
    });
  }

  getAllEvaluations(): Promise<Map<string, LetterEvaluation[]>> {
    return this.delay(
      () =>
        new Map(
          this.attendees
            .map(
              (a) =>
                [a.uuid, a.motivationLetterRatings ?? []] as [
                  string,
                  LetterEvaluation[]
                ]
            )
            .filter(([uuid, letters]) => letters.length > 0)
        )
    );
  }

  getEvaluations(authorEmail: string): Promise<Map<string, LetterEvaluation>> {
    return this.getAllEvaluations().then(
      (allEvals) =>
        new Map(
          Array.from(allEvals.entries())
            .map(([uuid, letterEvals]) => [
              uuid,
              letterEvals.filter(
                (e) => e.author?.email == this.currentUser?.email
              ),
            ])
            .filter(([uuid, letterEvals]) => letterEvals.length > 0)
            .map(
              ([uuid, letterEvals]) =>
                [uuid, letterEvals[0]] as [string, LetterEvaluation]
            )
        )
    );
  }

  getNewEvaluation(email: string): Promise<[string, LetterEvaluation]> {
    console.log("fakeClient: adding evaluation for " + email)
    return this.delay(() => {
      const attendee = randomFromArray(
        this.attendees
          .filter(a => a.registrationInfo?.motivationalLetter)
          .filter(
            (a) =>
              ((a.motivationLetterRatings ??
                []) as LetterEvaluation[]).filter((m) => m.author?.email == email).length == 0
          )
      );
      attendee.motivationLetterRatings = attendee.motivationLetterRatings ?? [];
      const rating = {
        author: this.getAttendeeByEmail(email)!,
        creativity: 0,
        grammar: 0,
        motivation: 0,
        karasIndex: 0,
      };
      attendee.motivationLetterRatings.push(rating);
      return [attendee.uuid, rating] as [string, LetterEvaluation];
    });
  }

  submitEvaluations(map: Map<string, LetterEvaluation>): Promise<void> {
    return this.delay(() => {
      for (const attendee of this.attendees) {
        if (map.has(attendee.uuid)) {
          const newEvaluation = map.get(attendee.uuid)!;
          attendee.motivationLetterRatings = [
            ...(
              attendee.motivationLetterRatings ?? ([] as LetterEvaluation[])
            ).filter((e) => e.author?.email != newEvaluation.author?.email),
            newEvaluation,
          ];
        }
      }
    });
  }

  deleteEvaluation(email: string, uuid: string): Promise<void> {
    return this.delay(() => {
      const attendee = this.attendees.find(a => a.uuid == uuid)
      if (attendee) {
        attendee.motivationLetterRatings = attendee.motivationLetterRatings?.filter(e => e.author?.email != email) ?? []
      }
    });
  }

  getMotivationLetterByUuid(uuid: string): Promise<Letter> {
    return this.delay(
      () => this.attendees.find(a => a.uuid == uuid)?.registrationInfo?.motivationalLetter!)
  }

  getAllAttendees(): Promise<Array<Attendee>> {
    return this.delay(() => this.attendees);
  }

  getAttendeeByEmail(email: string): Attendee | undefined {
    return this.attendees.find((a) => a.email == email);
  }

  getPositionDemand(): Promise<Map<string, number>> {
    return this.delay(() => new Map(demandEstimate(this.attendees, theCmak.positions)))
    // return this.delay(()=>{throw {reason:"spadlo to"}})
  }

  updateAttendee(person: Partial<Attendee>, id: string): Promise<Attendee> {
    if (!id) {
      id = this.currentUser?.id ?? ""
    }
    return this.delay(() => {
      const attendee = this.attendees.find(a => a.id == id)
      if (!attendee) {
        throw Error("Tento člověk neexistuje v databázi.")
      } else {
        Object.entries(person).forEach(([key, value]) => {
          attendee[key] = value;
        });
        console.log(attendee)
        return attendee
      }
    })
  }
  assignPositions(positions: Map<string, Position>): Promise<void> {

    return this.delay(() => {
      const map: { [key: string]: Position } = {}
      positions.forEach((position, key) => { map[key] = position })
      console.log("validating:", map)
      if (!validatePositionDistribution(map, theCmak.positions)) {
        console.log("failed")
        throw new Error("Neplatná distribuce rolí")
      }
      this.attendees.forEach(a => {
        if (positions.get(a.id)) {
          a.position = positions.get(a.id)
          // if(!a.authorizations?.includes(Authorization.ATTENDEE)){
          //   a.authorizations=(a.authorizations??[]).concat(Authorization.ATTENDEE)
          // }
        }
      })
    })
  }

  grantAttendeeStatus() {
    return this.delay(() => {
      console.log(this.attendees)
      const result = grantAttendees(this.attendees, theCmak.priceList)
      console.log(result)
      result.accepted.forEach(a => {
        console.log(theCmak.goodNewsEmailHtml(a.position!))
      })
      result.rejected.forEach(a => {
        console.log(theCmak.badNewsEmailHtml())
      })
    })
  }

  updatePayments(idsAndTotals: [string, number][]): Promise<void> {
    return this.delay(() => {
      idsAndTotals.forEach(([id, paid]) => {
        const a = this.attendees.find(a => a.id == id)!
        a.financeInfo!.paid = paid
      })
    })
  }

  getAttendeesToRate(): Promise<Attendee[]> {
    const attendeesToRate: Attendee[] = []
    const responsibilities = determineRatingResponsibility(this.currentUser!)
    if (responsibilities.adjustment.length || responsibilities.dailyRating.length || responsibilities.visibility.length) {
      const positionFilters: ApplicableFilter[] = ApplicableFilter.makeApplicable([...responsibilities.adjustment, ...responsibilities.dailyRating, ...responsibilities.visibility])
      console.log("Filters" + positionFilters)
      //let validAttendees: Attendee[];
      return this.getAllAttendees().then(attendees => {
        return attendees.filter((attendee) => isWithinFilters(attendee, positionFilters)
      );
      })
    }
    else{
      console.log("There were no responsibilities")
      console.log(theStore.person?.organizer?.orgPosition?.committee)
      return this.getAllAttendees().then(attendees => (attendees = attendeesToRate))
    } 
  }

  setDailyRating(attendeeId: string, day: number, rating: Rating): Promise<Attendee> {
    const attendee = this.attendees.find(a => a.id == attendeeId)
    if (!attendee) {
      throw new Error("Tento člověk neexistuje v databázi.")
    } else {
      return this.delay(() => {
        const dailyRating: DailyRating = { judge: this.currentUser!, rating: rating }
        if (attendee.gameRating) {
          if (attendee.gameRating.dailyRatings[day].find(rating => rating.judge = this.currentUser!)) {
            const index = attendee.gameRating.dailyRatings[day].indexOf(attendee.gameRating.dailyRatings[day].find(rating => rating.judge = this.currentUser!)!)
            attendee.gameRating.dailyRatings[day][index] = dailyRating
          } else {
            attendee.gameRating.dailyRatings[day].push(dailyRating)
          }
        } else {
          attendee.gameRating = { dailyRatings: new Array<DailyRating[]>([], [], []) }
          attendee.gameRating.dailyRatings[day].push(dailyRating)
        }
        return {...attendee}
      })
    }
  }

  setRatingAdjustment(attendeeId: string, adjustment: number): Promise<Attendee> {
    const attendee = this.attendees.find(a => a.id == attendeeId)
    return this.delay(() => {
      if (!attendee) {
        throw Error("Tento člověk neexistuje v databázi.")
      } else {
        attendee.gameRating!.adjustment = adjustment
        attendee.gameRating!.adjustmentAuthor = this.currentUser!
      }
      return {...attendee}
    })
  }

  constructor() {
    for (let i = 0; i < 250; i++) {
      this.attendees.push(randomAttendee(100, 100));
    }
    for (let i = 0; i < 20; i++) {
      const evaluator = randomAttendee(0, 0);
      evaluator.authorizations = [Authorization.EVALUATOR];
      this.evaluators.push(evaluator);
      this.attendees.push(evaluator)
    }
    for (let i = 0; i < 20; i++) {
      const admin = randomAttendee(0, 0);
      admin.authorizations = [Authorization.ADMIN];
      this.admins.push(admin);
      this.attendees.push(admin)
    }
    randomEvaluations([...this.attendees, ...this.evaluators], 1, 10);
  }
}

function randomString(length) {
  let result = "";
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}
