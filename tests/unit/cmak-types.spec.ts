
import {  CommitteeFilter, PartyFilter, StateFilter, SenateCommitteeId, Party, State, Position, Attendee, LobbyType, BodyType, LobbyistPosition  } from '@/../amplify/backend/function/cmakappLambdaLayer/opt/cmak-types'
import {  theCmak, countWords, satisfactionEstimate, isReadyForPosition, averageEval } from '@/../src/cmak-ui'
import 'jest'


const cmak = theCmak

describe('PositionFilters', () => {

  const senatePosition = cmak.senatePositions[0];
  const expectedSenateCommittee = senatePosition.committee;
  const expectedParty = senatePosition.party;
  const expectedState = senatePosition.state;

  const differentSenateCommittee = cmak.senateCommittees.filter(c => { return c.id != expectedSenateCommittee })[0].id;
  const differentState = State.Alaska == expectedState ? State.Alabama : State.Alaska;

  const committeeMatching: CommitteeFilter = new CommitteeFilter({ value: expectedSenateCommittee })
  const committeeNotMatching: CommitteeFilter = new CommitteeFilter({ value: differentSenateCommittee });
  const partyMatching: PartyFilter = new PartyFilter({ value: expectedParty })
  const partyNotMatching: PartyFilter = new PartyFilter({ value: expectedParty == Party.Republican ? Party.Democrat : Party.Republican });
  const stateMatching: StateFilter = new StateFilter({ value: expectedState });
  const stateNotMatching: StateFilter = new StateFilter({ value: differentState});

  it('filters senate positions by senate committee', () => {
    expect(committeeMatching.matches(senatePosition)).toBeTruthy()
    expect(committeeNotMatching.matches(senatePosition)).toBeFalsy()
  })

  it('filters senate positions by party', () => {
    expect(partyMatching.matches(senatePosition)).toBeTruthy()
    expect(partyNotMatching.matches(senatePosition)).toBeFalsy()
  })

  it('filters senate positions by state', () => {
    expect(stateMatching.matches(senatePosition)).toBeTruthy()
    expect(stateNotMatching.matches(senatePosition)).toBeFalsy()
  })
})


describe('QuillUtils', () => {

  const delta=[{insert:"ahoj prdelko"}]
  it('counts words', () => {
    expect(countWords(delta)).toBe(2)    
  })
})

describe('Satisfaction estimate', () => {
  const lobista:Attendee = {
    id: "aksdldkasj",
    uuid: "kajsdkasdjakdja",
    email: "lobista@n.com",
    name: "Lobista",
    pic: "zzzz",
    registrationInfo:{
      primaryRoles:[
        {
          body: BodyType.Lobby,          
          organization: {
            id: 'PA',
            name: 'Peace action',
            type: LobbyType.Un
          },
          identifier: `lobby:PA`
        } as LobbyistPosition
      ],
      secondaryRoleFilters: [
        {
          type: "body",
          value: "Senate"
        },
        {
          type: "committee",
          value: "VJ"
        },
        {
          type: "party",
          value: "Republican"
        },
        {
          type: "state",
          value: "Arizona"
        }
      ]
    }

  }

  const senatePosition={
    body: BodyType.Senate,
    committee: SenateCommitteeId.VOS,
    party: Party.Republican,
    state: State.Arizona,
    identifier: `senate:${Party.Republican}:${ SenateCommitteeId.VOS}:${State.Arizona}`
  }

  const expectedPosition = {
    body: BodyType.Lobby,          
    organization: {
      id: 'PA',
      name: 'Peace action',
      type: LobbyType.Un
    },
    identifier: `lobby:PA`
  } as LobbyistPosition

  // const topPositionMatching: Position = ({ body: expectedPosition.body, identifier: expectedPosition.identifier})
  it('gives 100% satisfaction for 1st role', () => {
    expect(satisfactionEstimate(lobista,expectedPosition!)).toBe(100);
  })

  it('gives right % satisfaction for matched filters', () => {
    expect(satisfactionEstimate(lobista,senatePosition)).toBe(63);
  })
})


describe('registration tools', () => {
  const lobista:Attendee = {
    id: "ejkfksfj",
    uuid: "kajsdkasdjakdja",
    email: "lobista@n.com",
    name: "Lobista",
    pic: "zzzz",
    registrationInfo:{
      primaryRoles:[
        {
          body: BodyType.Lobby,
          organization: {
            id: 'PA',
            name: 'Peace action',
            type: LobbyType.Un
          },
          identifier: `lobby:PA`
        } as LobbyistPosition
      ],
      secondaryRoleFilters: [
        {
          type: "body",
          value: "Senate"
        },
        {
          type: "committee",
          value: "VJ"
        },
        {
          type: "party",
          value: "Republican"
        },
        {
          type: "state",
          value: "Arizona"
        }
      ],
      motivationalLetter:{delta:{}}
    }
  }

  it('recognizes ready attendee',()=>{
    expect(isReadyForPosition(lobista)).toBeTruthy()
  })
  // const lobista2:Attendee={...lobista}
  // lobista2.registrationInfo!.primaryRoles=[]
  // it('recognizes nonready attendee',()=>{
  //   expect(isReadyForPosition(lobista2)).toBeFalsy()
  // })
  // const lobista3:Attendee={...lobista}
  // lobista3.registrationInfo!.primaryRoles=undefined
  // it('recognizes nonready attendee without roles',()=>{
  //   expect(isReadyForPosition(lobista3)).toBeFalsy()
  // })
  
})

describe('evaluation grammar',()=>{
  it('does not count zero evals into average',()=>{
    expect(averageEval([{creativity:0,grammar:0,karasIndex:0,motivation:0},{creativity:10,grammar:10,karasIndex:10,motivation:10}])).toEqual({creativity:10,grammar:10,karasIndex:10,motivation:10})
  })
})