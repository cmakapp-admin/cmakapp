import { Cached } from "@/../amplify/backend/function/cmakappLambdaLayer/opt/server-tools";
import {
  theCmak,
  countWords,
  satisfactionEstimate,
  isReadyForPosition,
} from "@/../src/cmak-ui";
import "jest";

describe("Cached", () => {
  let supplierCalled = false;
  const supplier = () => {
    supplierCalled = true;
    return new Promise((resolve) => resolve(3));
  };
  const cached = new Cached(supplier);
  it("calls supplier", () => {
    supplierCalled = false;
    return cached.get().then((x) => {
      expect(x).toBe(3);
      expect(supplierCalled).toBeTruthy();
    });
  });
  it("caches", () => {
    supplierCalled = false;
    return cached.get().then((x) => {
      expect(x).toBe(3);
      expect(supplierCalled).toBeFalsy();
    });
  });

  it("forgets", () => {
    supplierCalled = false;
    cached.stored=new Date(Date.now()-120000)
    return cached.get().then((x) => {
      expect(x).toBe(3);
      expect(supplierCalled).toBeTruthy();
    });
  });
});
