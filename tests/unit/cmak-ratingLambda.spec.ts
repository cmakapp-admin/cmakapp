//TODO - CmakTypes - IsWithinFilters
//TODO - Currently in RatingLambda - getValidatedDailyRatingPatch
import { Attendee, LobbyType, BodyType, LobbyistPosition, GameRating, DailyRating, LobbyRating } from '@/../amplify/backend/function/cmakappLambdaLayer/opt/cmak-types'
import { getValidatedDailyRatingPatch, getValidatedGameRating } from '@/../amplify/backend/function/cmakappLambdaLayer/opt/cmak-core'
import "jest";
import equal from "fast-deep-equal";

describe("validatedRatings", () => {
  let attendeeBase:Attendee = {
    id: "aksdldkasj",
    uuid: "kajsdkasdjakdja",
    email: "lobista@n.com",
    name: "Lobista",
    pic: "zzzz"  
  }
  let testGameRating:GameRating={
    dailyRatings: new Array<DailyRating[]>([], [], []) 
  }
  let attendeeWithoutRating:Attendee ={
        ...attendeeBase,       
    }
  let singleRating:LobbyRating = {
    type: "Lobby",
    overall: 4,
    readiness: 7,
    lobbing: 6,
    mediaImage: 23
  }
  let singleDailyRating:DailyRating = {
    judge: attendeeWithoutRating,
    rating: singleRating
  }  
  let filledGameRating:GameRating={
    ...testGameRating,
    ...testGameRating.dailyRatings[0][0] = singleDailyRating
  }
  let emptyGameRating:GameRating={
    ...testGameRating
  }
  let partialAttendeePatched:Partial<Attendee> = {
    id: attendeeBase.id,
    gameRating: filledGameRating
  }
    it('validates GameRating',()=>{
      expect(equal(getValidatedGameRating(attendeeWithoutRating),testGameRating)).toBeTruthy()
    })
    it('patches Attendee - validated', ()=>{
      expect(equal(getValidatedDailyRatingPatch(attendeeBase.id, emptyGameRating, attendeeWithoutRating, 0, singleRating),partialAttendeePatched)).toBeTruthy()
    })
  })