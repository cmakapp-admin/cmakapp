import { exportCsv } from "@/cmak-csv";

import "jest";

describe("exportCsv", () => {
  let supplierCalled = false;
  it('exports numbers',()=>{
    expect(exportCsv([{v:1},{v:2}],[["value",a=>a.v]])).toBe('"value"\n1\n2')
  })
  it('exports strings',()=>{
    expect(exportCsv([{v:1},{v:2}],[["value",a=>a.v],["value2",a=>('a'+a.v)]])).toBe('"value","value2"\n1,"a1"\n2,"a2"')
  })
})