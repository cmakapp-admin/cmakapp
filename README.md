# cmakproj

## Project setup
```
> git clone https://<user>@bitbucket.org/cmakapp/cmakapp.git
> npm install
```
on Windows - setup line endings for git:
```
> git config --local core.autocrlf input
```
make sure you are on the same branch (or close to it) that is deployed to the amplify env you work with - typically dev

```
> amplify pull
Scanning for plugins...
Plugin scan successful
? Select the authentication method you want to use: AWS profile

For more information on AWS Profiles, see:
https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html

? Please choose the profile you want to use cmakapp-dev
? Which app are you working on? d3lzaysgsx40aj
Backend environment 'dev' found. Initializing...
? Choose your default editor: Visual Studio Code
? Choose the type of app that you're building javascript
Please tell us about your project
? What javascript framework are you using vue
? Source Directory Path:  src
? Distribution Directory Path: dist
? Build Command:  npm.cmd run-script build
? Start Command: npm.cmd run-script serve
? Do you plan on modifying this backend? Yes
√ Successfully pulled backend environment dev from the cloud.


Successfully pulled backend environment dev from the cloud.
Run 'amplify pull' to sync upstream changes.
```

now we cleanup everything amplify pulled / modified locally that we want to keep in sync with git:

```
> git reset --hard
```
### Compiles and hot-reloads for development
```
> npm run serve
```

### Compiles and minifies for production
```
> npm run build
```

### Lints and fixes files
```
> npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## Amplify Lambda functions in TS

After amplify adds the lambda skeleton (when adding API, or otherwise), following steps will switch the lambda to Typescript. Right now the steps are mandatory, b/c the default .js implementation will not make it to Git (see .gitignore)

for function called XXX:

* copy `tsconfig.json` from another function folder to `amplify/backend/function/XXX/`
    * it should contain
    ``` "paths": {
        "src": ["./src"],
        "/opt/*": ["../cmakappLambdaLayer/opt/*"]        
      }
      ```
* move  `amplify/backend/function/XXX/src/index.js` to `amplify/backend/function/XXX/src/index.ts`
* in `amplify/backend/function/XXX/src/index.ts`:
    * change import and dynamo client declaration to
        ```
        import { CognitoIdentityServiceProvider, DynamoDB } from 'aws-sdk';
        const docClient = new DynamoDB.DocumentClient();
        ```
    * change handler declaration to 
         ```
        export const handler = async (event)...
        ```
    * add cmak imports
    import { Attendee } from "/opt/cmak-types";
    import { getCurrentUser, getCurrentAttendee } from "/opt/server-auth";

* add a line in root `package.json` :
    ```
    "amplify:XXX": "cd amplify/backend/function/XXX/src && npx tsc"
    ```

## Amplify push

Updating an environment

For backend part:
```
> amplify push 
```
after updating shared lambda layer (sometimes that happens seemingly unnecessarily) this appears at the beginning of amplify push output:
```
Content changes in Lambda layer cmakappLambdaLayer detected. Layer version increased to 13
Note: You need to run "amplify update function" to configure your functions with the latest layer version.
```

to update lambdas to newer layer version:

```
> amplify update function
? Select which capability you want to update: Lambda function (serverless function)
? Select the Lambda function you want to update cmakGetProfile
General information
| Name: cmakGetProfile
| Runtime: nodejs

Resource access permission
- cmakDynamo (create, read, update)
- cmakappf72350e2 (read)

Scheduled recurring invocation
| Not configured

Lambda layers

? Which setting do you want to update? Lambda layers configuration
? Do you want to configure Lambda layers for this function? Yes
? Provide existing layers or select layers in this project to access from this function (pick up to 5): cmakappLambdaLayer
? Select a version for cmakappLambdaLayer: 13
? Do you want to edit the local lambda function now? No
```

repeat for all impacted lambdas
then rerun the push