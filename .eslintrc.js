module.exports = {
    "parser": "vue-eslint-parser",
    "parserOptions": {
      "ecmaVersion": 7,
      "sourceType": "module"
    },
    "extends": ["@vue/typescript",'plugin:vue/essential'],
    "plugins": ["import", "vue"],
  }